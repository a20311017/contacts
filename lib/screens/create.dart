import 'package:flutter/material.dart';

class Create extends StatefulWidget {
  const Create({Key? key}) : super(key: key);

  @override
  State<Create> createState() => _CreateState();
}

class _CreateState extends State<Create> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Add New Contact'),
          actions: [
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text("Guardando...")));
                      }
                    },
                    icon: const Icon(Icons.check),
                    tooltip: "Save"))
          ],
        ),
        body: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 4.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextFormField(
                    decoration: const InputDecoration(hintText: "Nombre"),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "favor de ingresar el nombre de contacto";
                      } else if (value.length > 4) {
                        return "El largo del nombre es menor a 4";
                      }
                      return null;
                    }),
                TextFormField(
                    decoration: const InputDecoration(hintText: "Numero"),
                    keyboardType: TextInputType.phone,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "favor de ingresar el telefono del contacto";
                      } else if (value.length > 10) {
                        return "favor de ingresar un numero de 10 digitos";
                      }
                      return null;
                    }),
                TextFormField(
                    decoration: const InputDecoration(hintText: "Correo"),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "favor de ingresar el correo del contacto";
                      }
                      return null;
                    }),
                // TextFormField(
                //     decoration: const InputDecoration(
                //       hintText: "Cumpleaños"
                //     ),validator: (value) {
                //   if(value==null || value.isEmpty){
                //     return "favor de ingresar el cumpleaños del contacto";
                //   }else if(value.length > 10){
                //   }
                //   return null;
                // }),
                InputDatePickerFormField(
                    fieldLabelText: "Cumpleaños",
                    firstDate: DateTime(DateTime.now().year - 120),
                    lastDate: DateTime.now())
              ],
            ),
          ),
        ));
  }
}
