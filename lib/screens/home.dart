import 'dart:typed_data';

import 'package:flutter/material.dart';
//import 'package:flutter_contacts/contacts.dart';
import 'package:flutter_contacts/flutter_contacts.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Contact>? contacts;

  @override
  void initState() {
    super.initState();
    getContacts();
  }

  getContacts() async {
    if (await FlutterContacts.requestPermission()) {
      contacts = await FlutterContacts.getContacts(
          withProperties: true, withPhoto: true);
      //print on console
      print(contacts);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Contacts'),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.search),
            tooltip: "Search Contacts",
          )
        ],
      ),
      body: (contacts) == null
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: contacts!.length,
              itemBuilder: (BuildContext context, int index) {
                Contact contacto = contacts![index];
                Uint8List? imageCont = (contacto.photo);
                String num = (contacto.phones.isNotEmpty)
                    ? (contacto.phones.first.number)
                    : '--';

                return Card(
                    child: InkWell(
                  onTap: () {
                    Navigator.pushNamed(context, '/view', arguments: contacto);
                  },
                  splashColor: Colors.deepPurple[30],
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        leading: (imageCont != null)
                            ? CircleAvatar(
                                radius: 28,
                                backgroundImage: MemoryImage(imageCont),
                                backgroundColor: Colors.transparent,
                              )
                            : const CircleAvatar(
                                radius: 28,
                                backgroundColor: Colors.deepPurple,
                                child: Icon(Icons.person),
                              ),
                        //Text('DG',style: TextStyle(fontSize: 28),)
                        title: Text(
                            '${contacts![index].name.first} ${contacts![index].name.last}'),
                        subtitle: Text(num),
                      ),
                    ],
                  ),
                ));
              },
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //Navigator.pop(context);
          Navigator.pushNamed(context, '/add');
        },
        tooltip: 'Add Contact',
        child: const Icon(Icons.add),
      ),
    );
  }
}
