import 'package:flutter/material.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:url_launcher/url_launcher.dart';

class View extends StatefulWidget {
  const View({Key? key}) : super(key: key);

  @override
  State<View> createState() => _ViewState();
}

class _ViewState extends State<View> {
  @override
  void initState() {
    super.initState();
  }

  _launchCaller(tel) async {
    tel = "tel:" + tel;
    if (await canLaunch(tel)) {
      await launch(tel);
    } else {
      throw 'Could not launch $tel';
    }
  }

  // getContact(contacto) async {
  //   if (await FlutterContacts.requestPermission()) {
  //     contacto = await FlutterContacts.getContact(contacto.id);
  //     setState(() {});
  //   } else {
  //     contacto = Contact(
  //         displayName: "Unkown Contact",
  //         emails: [Email('correo@correo.com', label: EmailLabel.home)]);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    final Contact contacto =
        ModalRoute.of(context)!.settings.arguments as Contact;
    //getContact(contacto);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Contacts'),
      ),
      body: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.bottomLeft,
            children: [
              (contacto.photo == null)
                  ? Image.asset('assets/img/contact-placeholder.jpg')
                  : Image(image: MemoryImage(contacto.photo!)),
              Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 8.0),
                child: (contacto.displayName.isNotEmpty)
                    ? Text(contacto.displayName,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 30))
                    : const Text('Unkown Contact',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 30)),
              ),
            ],
          ),
          (contacto.phones.isEmpty)
              ? const Card(
                  child:
                      Text('Add Phonenumber', style: TextStyle(fontSize: 18)))
              : Card(
                  child: Row(children: [
                    Row(children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: (contacto.phones.isNotEmpty)
                                ? Text(contacto.phones.first.normalizedNumber,
                                    style: const TextStyle(fontSize: 18))
                                : const Text('',
                                    style: TextStyle(fontSize: 18)),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: (contacto.phones.isNotEmpty)
                                ? Text(contacto.phones.first.customLabel,
                                    style: const TextStyle(
                                        fontSize: 14, color: Colors.grey))
                                : const Text('',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.grey)),
                          )
                        ],
                      )
                    ]),
                    Expanded(
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: IconButton(
                                onPressed: () {
                                  if (contacto.phones.isNotEmpty) {
                                    _launchCaller(
                                        contacto.phones.first.normalizedNumber);
                                  }
                                },
                                icon: const Icon(Icons.phone),
                                tooltip: "Dial Number",
                              ),
                            ),
                            const Icon(Icons.message),
                            const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Icon(Icons.whatsapp),
                            )
                          ]),
                    )
                  ]),
                ),
          (contacto.emails.isEmpty)
              ? const Card(
                  child: Text('Add Email', style: TextStyle(fontSize: 18)))
              : Card(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: (contacto.emails.isNotEmpty)
                                        ? Text(contacto.emails.first.address,
                                            style:
                                                const TextStyle(fontSize: 16))
                                        : const Text('',
                                            style: TextStyle(fontSize: 16)),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: (contacto.emails.isNotEmpty)
                                        ? Text(contacto.emails.first.label.name,
                                            style: const TextStyle(
                                                fontSize: 14,
                                                color: Colors.grey))
                                        : const Text('',
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Colors.grey)),
                                  )
                                ],
                              )
                            ]),
                        Expanded(
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: const [
                                Padding(
                                  padding: EdgeInsets.all(16.0),
                                  child: Icon(Icons.mail),
                                )
                              ]),
                        )
                      ]),
                ),
          Card(
            child: Row(children: [
              Row(children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text('April 30', style: TextStyle(fontSize: 18)),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text('Birthday',
                          style: TextStyle(fontSize: 14, color: Colors.grey)),
                    )
                  ],
                )
              ]),
              Expanded(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: const [
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Icon(Icons.calendar_month),
                      )
                    ]),
              )
            ]),
          )
        ],
      ),
    );
  }
}
