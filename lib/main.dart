import 'package:flutter/material.dart';
import 'package:flutter_contacts/contact.dart';
import 'package:mycontacts/screens/home.dart';
import 'package:mycontacts/screens/create.dart';
import 'package:mycontacts/screens/view.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Contacts',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: const Home(),
      routes: {
        '/home': (context) => const Home(),
        '/add': (context) => const Create(),
        '/view': (context) => const View(),
      },
    );
  }
}
